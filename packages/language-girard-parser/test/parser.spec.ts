import { parse } from "../src"
import * as E from 'fp-ts/Either'
import { pipe } from 'fp-ts/pipeable'

const exampleTerm: string = 'a -> b -> a b b'

test(`Parse example term: ${exampleTerm}`, () =>
  pipe(
    parse(exampleTerm),
    E.fold(
      err => {
        console.error(err)
        throw Error(err)
      },
      tm => console.log(JSON.stringify(tm))
    )
  )
)
