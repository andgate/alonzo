import { pipe } from 'fp-ts/lib/pipeable'
import * as C from 'parser-ts/char'
import * as S from 'parser-ts/string'
import * as P from 'parser-ts/Parser'
import { Term, termApp, TermApp, TermLam, termLam, termVar, TermVar } from 'language-girard-syntax/lib/Term'
import { freeVar, Var } from 'language-girard-syntax/lib/Var'

// const equalsP: P.Parser<string, string> = C.char('=')
const arrowRightP: P.Parser<string, string> = S.string('->')
const symbolP: P.Parser<string, string> = C.many1(C.alphanum)

const varP = (): P.Parser<string, Var> => 
  pipe(
    symbolP,
    P.map(symbol => freeVar(symbol))
  )

export const termP = (): P.Parser<string, Term> =>
  P.either(
    termLamP(),
    () => P.either<string, Term>(
      termAppP(),
      () => atermP()
    )
  )

const atermP = (): P.Parser<string, Term> =>
  P.either(termVarP(), () => termParensP())

const termVarP = (): P.Parser<string, TermVar>  =>
  pipe(
    varP(),
    P.map(x => termVar(x))
  )

const termParensP = (): P.Parser<string, Term> =>
  pipe(
    C.char('('),
    P.chain(() => S.spaces),
    P.chain(() => termP()),
    P.chain((tm) => 
      pipe(
        C.char(')'),
        P.map(_ => tm)
      )
    )
  )

const termAppP = (): P.Parser<string, TermApp> =>
  pipe(
    P.either(termVarP(), () => termParensP()),
    P.chain((termA) =>
      pipe(
        S.spaces,
        P.chain(() => P.either(termVarP(), () => termParensP())),
        P.map((termB) => termApp(termA, termB))
      )
    )
  )

const termLamP = (): P.Parser<string, TermLam> => 
  pipe(
    symbolP,
    P.chain(x => 
      pipe(
        S.spaces,
        P.chain(() => arrowRightP),
        P.chain(() => S.spaces),
        P.chain(() => termP()),
        P.map(tm => 
          termLam(x, tm)
        )
      )
    )
  )
