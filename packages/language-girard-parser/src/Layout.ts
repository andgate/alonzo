export type Layout<A> = Block<A> | LineFold<A>

export type Block<A> = {
  _tag: 'TokenBlock'
  lines: LineFold<A>[]
}

export type LineFold<A> = {
  _tage: 'TokenLineFold'
  tokens: A[]
}
