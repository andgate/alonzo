// https://lihautan.com/step-by-step-guide-for-writing-a-babel-transformation/

export default function GirardBabelPlugin() {
  return {
    visitor: {
      Identifier(path: { isIdentifier: (arg0: { name: string; }) => any; node: { name: string; }; }) {
        // in this example change all the variable `n` to `x`
        if (path.isIdentifier({ name: 'n' })) {
          path.node.name = 'x';
        }
      },
    },
  };
}
