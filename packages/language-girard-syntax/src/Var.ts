export type Var = BoundVar | FreeVar

export type BoundVar = {
  _tag: 'BoundVar'
  id: number
  symbol: string
}

export type FreeVar = {
  _tag: 'FreeVar'
  symbol: string
}

export const freeVar = (symbol: string): FreeVar => ({
  _tag: 'FreeVar', 
  symbol
})

export const boundVar = (freeVar: FreeVar, id: number): BoundVar => ({
  _tag: 'BoundVar',
  id,
  symbol: freeVar.symbol
})
