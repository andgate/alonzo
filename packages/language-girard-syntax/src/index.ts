import * as Scope from './Scope'
import * as Term from './Term'
import * as Var from './Var'

export default {
  ...Scope,
  ...Term,
  ...Var
}