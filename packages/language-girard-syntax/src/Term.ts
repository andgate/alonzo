import { Var } from './Var'
import { scope, Scope } from './Scope'

export type Term =
    TermVar 
  | TermApp
  | TermLam

export type TermVar = { 
  _tag: 'TermVar'
  variable: Var
}

export type TermLam = {
  _tag: 'TermLam'
  scope: Scope
}

export type TermApp = {
  _tag: 'TermApp'
  termA: Term
  termB: Term
}

export type TermInfo = {
  hasFree: boolean
  maxBind: number
}


export const termVar = (variable: Var): TermVar => ({
  _tag: 'TermVar',
  variable
})

export const termApp = (termA: Term, termB: Term): TermApp => ({
  _tag: 'TermApp',
  termA,
  termB
})

export const termLam = (variable: string, body: Term): TermLam => ({
  _tag: 'TermLam',
  scope: scope(variable, body)
})