const path = require('path');

module.exports = {
  entry: {
    'language-girard-compiler': './src/index.ts',
    'language-girard-compiler.min': './src/index.ts'
  },
  output: {
    path: path.resolve(__dirname, '_bundles'),
    filename: '[name].js',
    libraryTarget: 'umd',
    library: 'LanguageGirardCompiler',
    umdNamedDefine: true
  },
  devtool: 'source-map',
  module: {
    rules: [
      {
        // Include ts, tsx, js, and jsx files.
        test: /\.(ts|js)$/,
        exclude: /node_modules/,
        loader: 'babel-loader',
      }
    ],
  },
  resolve: {
    extensions: [ '.ts', '.js' ]
  }
};
