const ExecutableScript = require('./webpack/executable-script-webpack-plugin');
const path = require('path');
const webpack = require('webpack');

module.exports = {
  entry: './src/server.ts',
  target: 'node',
  output: {
    path: path.resolve(__dirname, 'bin'),
    filename: 'server.js',
  },
  module: {
    rules: [
      {
        // Include ts, tsx, js, and jsx files.
        test: /\.(ts|js)$/,
        exclude: /node_modules/,
        loader: 'babel-loader',
      }
    ],
  },
  resolve: {
    extensions: [ '.ts', '.js' ]
  },
  plugins: [
    new ExecutableScript('bin/server.js'),
    new webpack.BannerPlugin({
      banner: '#!/usr/bin/env node',
      raw: true,
      entryOnly: true
    }),
  ]
};
