let
  forall (l: level) (a: type l).
  functor (F: type l -> type l) (a: type l): constraint l = {
    fmap: (f: a -> b) -> (fa: F a) -> F b
  }
in
{ functor }