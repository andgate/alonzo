forall (l: level).
let
  list (a: type l): type l = { nil: nil }|{ cons: cons } 
  nil (a: type l): type l = {}
  cons (a: type l): type l = { head: a, tail: list a }

  forall (a: type l) (b: type l).
  map (f: (_: a) -> b) (xs: list a): list b =
    case xs of
      {nil} -> {nil}
      {cons} -> {
        head: f cons.head,
        tail: map f cons.tail
      }
in {
  list,
  nil,
  cons,
  map
}
