forall (l: Level).
let
  pair (a: type l) (b: type l): type l = [a, b]
  
  forall (a: type l) (b: type l).
  first (p: pair a b): a = p[0]

  forall (a: type l) (b: type l).
  second (p: pair a b): b = p[1]

  forall (a: type l) (b: type l) (c: type l).
  mapFirst (f: a -> c) (p: pair a b): pair c b =
    [f p.first, p.second]

  forall (a: type l) (b: type l) (d: type l).
  mapSecond (g: b -> d) (p: pair a b): pair a d =
    [p.first, g p.second]
in {
  pair,
  first,
  second,
  mapFirst
  mapSecond
}