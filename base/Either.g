forall (l: Level).
let
  either (a: type l) (b: type l) = {left: a}|{right: b}

  forall (e: type l) (a: type l) (b: type l).
  map (f: a -> b) (ea: either e a): either e b = 
    case ea of
      {left} -> {left}
      {right} -> { right: f right }
in {
  either,
  map
}
