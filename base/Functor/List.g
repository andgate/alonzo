import '../List.g' in
import '../Functor.g' in
let
  forall (l: level) (a: type l)
  listFunctor: functor list a = {
    fmap: map
  }
in { listFunctor }
